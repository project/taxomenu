<?php

/**
 * @file taxomenu.module
 *
 * Integration of taxonomy with menu, using panel pages 
 * for display. Can be seen as an integration of 
 * the taxonomy_menu and taxonomy_assoc modules.
 * By relying on a patch to menu.inc (included), the URLs
 * generated can be flat, ie. PANEL-URL/TERM-ID
 * 
 * The module associates a menu entry with each taxonomy terms
 * in a configurable vocabulary. 
 * As callbacks, panel pages are used to display stuff (eg. nodes 
 * associated with the term.
 * 
 * Topics, ressorts etc. are refered to as channels in this module.
 */

/**
 * Helper function that returns the list of available panel pages as options
 * array for forms-api
 */
function _taxomenu_pages($with_default = FALSE) {
  $options = $with_default ? array('--' => '[default]') : array();

  // load panels from database
  $result = db_query("SELECT pid, title FROM {panels_page}");
  while ($page = db_fetch_object($result)) {
    $options[$page->pid] = $page->title;
  }
  return $options;  
}


/**
 * Helper function that gets the title of a panel page
 */
function taxomenu_panel_page_title($pid) {
  static $title;
  if (!isset($title)) {
    $title = array();
  }
  if (!$pid) {
    return '--';
  }
  if (isset($title[$pid])) {
    return $title[$pid];
  }
  $page = db_fetch_object(db_query('SELECT title FROM {panels_page} WHERE pid=%d', $pid));
  $title[$pid] = $page->title;

  return $title[$pid];
}


/**
 * 1. Extend form 'taxonomy-form-term' at admin/content/taxonomy/edit/term/TID
 *    to associate a node with a term
 * 2. Extend form 'taxonomy_form_vocabulary'
 *    to add "Manage by Tax'o'Menu"
 */
function taxomenu_form_alter($form_id, &$form) {
  switch ($form_id) {
  case 'taxonomy_form_term':
    $result = db_query('SELECT p.pid, p.title FROM {panels_page} p LEFT JOIN {term_data} t ON p.pid = t.pid WHERE t.tid = %d',
      $form['tid']['#value']);
    $page = db_fetch_object($result);

    $form['page'] = array(
      '#type' => 'select',
      '#title' => t('Page'),
      '#options' => _taxomenu_pages(TRUE),
      '#weight' => 0,
      '#description' => t('This page will be displayed for this term'),
      '#default_value' => $page->pid,
    );

    $form['#submit']['taxomenu_assoc_page'] = array();

  case 'taxonomy_form_vocabulary':
    $taxomenu = $form['module']['#value'] == 'taxomenu';
    
    $form['taxomenu'] = array(
      '#type' => 'checkbox',
      '#title' => t("Manage by Tax'o'Menu'"),
      '#description' => t('If enabled, taxonomy terms are display in a panel page.'),
      '#weight' => 0.0085, // Try to have this show up after the 'Required' checkbox
      '#default_value' => $taxomenu,
    );
    
    $form['#submit']['taxomenu_assoc_vocabulary'] = array();
  }
}

/**
 * Form submit handler.
 * Associates a vocabulary with this module
 */
function taxomenu_assoc_vocabulary($form_id, $form_values) {
  $vid = $form_values['vid'];
  
  if ($form_values['taxomenu']) {
    if ($form_values['module'] != 'taxomenu') {

      // this hopefully (set system.weight high enough) comes after all other modules
      db_query("UPDATE {vocabulary} SET module = '%s' WHERE vid = %d", 'taxomenu', $vid);
      drupal_set_message(t('Taxonomy terms are now displayed on panel pages.'));
    }
  }
  else {
    if ($form_values['module'] == 'taxomenu') {
      db_query("UPDATE {vocabulary} SET module = '%s' WHERE vid = %d", 'taxonomy', $vid);
      drupal_set_message(t('Taxonomy terms are now displayed as a list of nodes.'));
    }      
  }
}

/**
 * Form submit handler.
 * Associates a term with a node
 */
function taxomenu_assoc_page($form_id, $form_values) {
  $pid = intval($form_values['page']);
  //D error_log(sprintf('[assoc] tid=%d pid=%d', $form_values['tid'], $pid));
  db_query('UPDATE {term_data} SET pid = %d WHERE tid = %d', $pid, $form_values['tid']);
  return FALSE; // no redirect
}

/**
 * Implementation of hook_menu().
 */
function taxomenu_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[]= array (
      'path' => 'admin/settings/nc/menu',
      'title' => t("Tax'o'Menu"),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('taxomenu_admin_settings'),
      'access' => user_access('administer site configuration'),
      'description' => t("Global configuration of Tax'o'Menu menu functionality."),
      'type' => MENU_NORMAL_ITEM,
    );

    $vocabulary_vid = variable_get('taxomenu_show_vocabulary', 1);
    $toplevel_tid = variable_get('taxomenu_show_term', 1);

    $max_depth = variable_get('taxomenu_max_depth', 0);
    $max_depth = $max_depth ? $max_depth : NULL;    

    $tree = taxonomy_get_tree($vocabulary_vid, $toplevel_tid, -1, $max_depth);

    $access = user_access('access content');

    /*
    $old_depth = -1;
    $old_path = '';
     */
    $old_depth = 0;
    $path = array();
     
    foreach ($tree as $i => $term) {
      $page_id = $term->pid ? $term->pid : variable_get('taxomenu_default_page', 0);
      $page = panels_page_load($page_id);

//D var_dump(array('tid'=>$term->tid, 'pid'=>$term->pid, 'pid'=>$page->pid));

      /*
       * The original approach - taken from taxonomy_menu - uses
       * hierarchical pathes to reflect the hierarchy.
       * To be able to use arbitrary pathes for panel_pages, we want
       * to create hierarchy directly by setting the pid. Thus, we need the
       * mid of newly created items which we yield by saving the items.
       *
      
      if ($term->depth <= $old_depth) {
        $slashes_to_remove = $old_depth - $term->depth + 1;
        for ($i = 0; $i < $slashes_to_remove; $i++) {
          $old_path = substr($old_path, 0, strrpos($old_path, '/'));
        }
      }
      $path = $old_path .'/'. $term->tid;
      $old_depth = $term->depth;
      $old_path = $path;
       */
      
      /*
       * The new approach works with menu.inc patched as of 
       *   http://drupal.org/node/103539
       */
      if ($term->depth > $old_depth) {
        array_unshift($path, $old_path);
      }
      else {
        while ($term->depth < $old_depth) {
          array_shift($path);
          $old_depth--;
        }
      }
      $old_path =  taxomenu_term_path($term, $page);
      $old_depth = $term->depth;      

      $item = array('path' => $old_path, 
        'title' => t($term->name),
        'weight' => $term->weight,
        'access' => $access,
        'callback' => 'panels_page_view_page',
        'callback arguments' => array(
          $page->pid, array('title' => t($term->name))),
        'type' => MENU_NORMAL_ITEM | MENU_EXPANDED,
      );
      if ($term->depth) {
        $item['parent'] = $path[0];
      }
      else {
        $item['pid'] = variable_get('taxomenu_show_menu', 1);
      }
      $items[] = $item;
//D var_dump($item);
    }
  }
  return $items;
}

function taxomenu_primary_term($node) {
  $frontpage = split('/', variable_get('site_frontpage', 'channel/0'));
  $frontpage_tid = isset($frontpage[1]) && is_numeric($frontpage[1])
    ? $frontpage[1] : 0;

  $vid = variable_get('taxomenu_show_vocabulary', 1);

  foreach ($node->taxonomy as $tid => $term) {
    if ($term->vid != $vid) {
      continue;
    }
    if ($tid == $frontpage_tid) {
      // skip frontpage channel
      continue;
    }
    return $term;
  }   
  return NULL;
}

/**
 * Make stories appear in the first channel that is not the site's frontpage.
 * Best called in node.tpl.php
 */
function taxomenu_place_node_in_menu($node) {
  $term = taxomenu_primary_term($node);
  if (!$term) {
    return;
  }
  
  $term_path = taxonomy_term_path($term);
        
  $location = array(
    array('path' => $term_path),
    array('path' => arg(0) . '/' . arg(1)));
    
  //D var_dump($location);

  // make channel menu parent of this story
  menu_set_location($location);
}

/**
 * Implementation of hook_taxonomy().
 *
 * Invalidates the menu cache on taxonomy changes.
 */
function taxomenu_taxonomy() {
  menu_rebuild();
}

/**
 * Menu callback; manage settings for taxonomy menu.
 */
function taxomenu_admin_settings() {
  $options = array();
  foreach (taxonomy_get_vocabularies() as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name;
  }
  $form['taxomenu_show_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#default_value' => variable_get('taxomenu_show_vocabulary', 1),
    '#options' => $options,
  );

  // TODO: If vocabulary changes, update available terms
  // OR, make form multipart.

  $vocabulary_vid = variable_get('taxomenu_show_vocabulary', 1);
  $options = array();
  foreach (taxonomy_get_tree($vocabulary_vid, 0, -1, 1) as $term) {
    $options[$term->tid] = $term->name;
  }
  $form['taxomenu_show_term'] = array(
    '#type' => 'select',
    '#title' => t('Toplevel term'),
    '#default_value' => variable_get('taxomenu_show_term', 1),
    '#options' => $options,
  );

  $form['taxomenu_max_depth'] = array(
    '#type' => 'textfield',
    '#size' => 2,
    '#title' => t('Number of levels'),
    '#default_value' => variable_get('taxomenu_max_depth', 2),
    '#description' => t('0 for "all levels"'),
  );

  $options = array();
  $menu = menu_get_menu();
//var_dump($menu['visible']);
  foreach ($menu['visible'][0]['children'] as $mid) {
    $options[$mid] = $menu['visible'][$mid]['title'];
  }
  $form['taxomenu_show_menu'] = array(
    '#type' => 'select',
    '#title' => t('Menu'),
    '#default_value' => variable_get('taxomenu_show_menu', 1),
    '#options' => $options,
  );

  $form['taxomenu_default_page'] = array(
    '#type' => 'select',
    '#title' => t('Default panel page'),
    '#options' => _taxomenu_pages(),
    '#default_value' => variable_get('taxomenu_default_page', 0),
    '#description' => t('This panel page will be displayed for terms that have no panel page associated with them'),
   );

  $form['taxomenu_story_base'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Base fragement of story URL'),
    '#default_value' => variable_get('taxomenu_story_base', 'story'),
    '#description' => t('Story URLs have to be constructed as FRAGMENT/NID'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_term_path() from taxonomy.
 * @param $page purely optimization if caller already has page
 */
function taxomenu_term_path($term, $page = NULL) {
  if (!$page) {
    $page_id = $term->pid ? $term->pid : variable_get('taxomenu_default_page', 0);
    $page = panels_page_load($page_id);
  }

  if (strpos($page->path, '%') !== FALSE) {
    return strtr($page->path, array('%' => $term->tid));
  }
  else {
    return $page->path . '/' . $term->tid;
  }
}

